using System;
using Xunit;
using RPGCharactersAss1;

namespace RPGCharactersAss1Tester
{
    /// <summary>
    /// Tests for character
    /// </summary>
    public class CharacterTests
    {
        /*
         Methods test nameing
         The name of the method being tested.
         The scenario under which it's being tested.
         The expected behavior when the scenario is invoked.
        */
        /// <summary>
        /// Create character, characer level should be 1, check if character level == 1
        /// </summary>
        [Fact]
        public void CreateCharacter_CheckLevel_ReturnLevelOne()
        {
            // Setup
            Character warrior = new Warrior();
            // Actual = What were going to test
            var actual = warrior.Level();
            // Expected = What we expect
            var expected = 1;
            // Check if actual is what we expected, else fail test
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Create character, level up character, check if character level == 2
        /// </summary>
        [Fact]
        public void LevelUpCharacter_CheckLevel_LevelTwo()
        {
            // arrange
            Warrior warrior = new Warrior();
            // act
            warrior.LevelUp();
            var actual = warrior.Level();
            var expected = 2;
            // assert
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Create character subclass, Get characterStats from subclass (Unic for every subclass), 
        /// Check if characterStats are correct given subclass.
        /// </summary>
        /// <param name="characterType">Specify sublass to create</param>
        /// <param name="expected">Expected characterStats for subclass</param>
        // Theroy allows you do use "InlineData" that can take parameter.
        [Theory]
        // Set parameter for test
        [InlineData(CharacterType.MAGE,    new float[]{ 1, 1, 8 })]
        [InlineData(CharacterType.WARRIOR, new float[]{ 5, 2, 1 })]
        [InlineData(CharacterType.RANGER,  new float[]{ 1, 7, 1 })]
        [InlineData(CharacterType.ROGUE,   new float[]{ 2, 6, 1 })]
        public void CreateSubclasses_CheckStartingStats_SubclassAttributes(CharacterType characterType, float[] expected)
        {
            Character character = CreateClassesForTests.InitCharacter(characterType);
            float[] actual = { character.Stats().Strength(), character.Stats().Dexterity(), character.Stats().Intelligence() };

            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Create character, Level up character 0-* times, Check if characterStats are equal to expected CharacterStats
        /// </summary>
        /// <param name="characterType">Specify sublass to create</param>
        /// <param name="nrOfLevelUps">How many times to level up character</param>
        /// <param name="baseStats">CharacterStats for character when created</param>
        /// <param name="levelUpStats">CharacterStats character gain every time character levels up</param>
        [Theory]
        [InlineData(CharacterType.MAGE,    1,   new float[] { 1, 1, 8 }, new float[] { 1, 1, 5 })]
        [InlineData(CharacterType.WARRIOR, 1,   new float[] { 5, 2, 1 }, new float[] { 3, 2, 1 })]
        [InlineData(CharacterType.RANGER,  1,   new float[] { 1, 7, 1 }, new float[] { 1, 5, 1 })]
        [InlineData(CharacterType.ROGUE,   1,   new float[] { 2, 6, 1 }, new float[] { 1, 4, 1 })]
        [InlineData(CharacterType.ROGUE,   2,   new float[] { 2, 6, 1 }, new float[] { 1, 4, 1 })]
        [InlineData(CharacterType.ROGUE,   3,   new float[] { 2, 6, 1 }, new float[] { 1, 4, 1 })]
        [InlineData(CharacterType.ROGUE,   100, new float[] { 2, 6, 1 }, new float[] { 1, 4, 1 })]
        public void LevelUp_SubclassStats_ReturnStats(CharacterType characterType, int nrOfLevelUps, float[] baseStats, float[] levelUpStats)
        {
            /*  
            Very little code that makes it fast and safe to test 
            if characer have level up correctly. Even if thats 100 levels.
            BaseStats + (LevelUpStats * NumberOfTimesCharacterLevelsUp) 
            */
            Character character = CreateClassesForTests.InitCharacter(characterType);
            // Run Level up on character <nrOfLevelUps> of times
            for (int i = 0; i < nrOfLevelUps; i++)
            {
                // Level up character
                character.LevelUp();
            }
            // Get characterStats from character
            float[] actual = { character.Stats().Strength(), character.Stats().Dexterity(), character.Stats().Intelligence() };

            float[] expected = { 
                // OncreateStats + (LevelupStats * NrOfLevels)
                baseStats[0] + (levelUpStats[0] * nrOfLevelUps), 
                baseStats[1] + (levelUpStats[1] * nrOfLevelUps), 
                baseStats[2] + (levelUpStats[2] * nrOfLevelUps) 
            };
            Assert.Equal(expected, actual);
        }

    }
}
