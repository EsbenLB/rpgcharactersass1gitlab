using System;
using Xunit;
using RPGCharactersAss1;

namespace RPGCharactersAss1Tester
{
    /// <summary>
    /// Tests for Items
    /// </summary>
    public class ItemTests
    {
        #region Item Level too high
        /// <summary>
        /// Give character a Weapon with level above characters level. 
        /// This should throw an InvalidItemException
        /// </summary>
        [Fact]
        public void WeaponEquip_ItemTooHighLevel_Exception()
        {
            // Variable required for this test
            int level = 2;
            // Create default warrior class
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            // Create Weapon with level=2
            Weapon testAxe = new (EquipmentType.WEAPON, WeaponType.AXES, itemLevel: level);

            // Check if EquipWeapon throws InvalidItemException
            // Don't call method inside of Assert. Onless you expect Exception response
            // InvalidItemException instead of InvalidWeaponException, bechoose we are checking for item attributes not Armor attributes
            Assert.Throws<InvalidItemException>(() => warrior.EquipWeapon(testAxe));

        }
        /// <summary>
        /// Give character a Armor with level above characters level. 
        /// This should throw an InvalidItemException
        /// </summary>
        [Fact]
        public void ArmorEquip_ItemTooHighLevel_Exception()
        {
            int level = 2;
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            // Get armor
            Armor testArmor = new (EquipmentType.BODY, ArmorType.PLATE, itemLevel: level);

            Assert.Throws<InvalidItemException>(() => warrior.EquipArmor(testArmor));
        }
        #endregion
        #region Wrong Item type for subclass
        /// <summary>
        /// Try to equip weapon on character from a another subclass.
        /// This should throw an InvalidWeaponException
        /// </summary>
        [Fact]
        public void EquipWeapon_WrongWeaponType_Exception()
        {
            // Variable to test. 
            WeaponType weaponType = WeaponType.BOWS;
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Weapon testAxe = new (EquipmentType.BODY, weaponType);
            // Warrior can't equip BOWS
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }
        /// <summary>
        /// Try to equip armor on character from a another subclass.
        /// This should throw an InvalidArmorException
        /// </summary>
        [Fact]
        public void EquipArmor_WrongArmorType_Exception()
        {
            ArmorType armorType = ArmorType.CLOTH;
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Armor testArmor = (Armor)CreateClassesForTests.InitItems(armorType: armorType);
            // Warrior can't equip CLOTH
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testArmor));
        }
        #endregion
        #region Correct Item type for subclass
        /// <summary>
        /// Equip character with correct weapon for subclass.
        /// Method EquipWeapon() should return string ~"Success"
        /// </summary>
        [Fact]
        public void EquipWeapon_CorrectType_Success()
        {
            // Variable to test.
            WeaponType weaponType = WeaponType.AXES;
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Weapon testAxe = new(EquipmentType.BODY, weaponType);
            // EquipWeapon() should return string ~"success" if weapon got equiped
            var actual = warrior.EquipWeapon(testAxe);
            // Warrior can equip weapon of type AXES
            Assert.Equal("New weapon equipped!", actual);
        }
        /// <summary>
        /// Equip character with correct armor for subclass.
        /// Method EquipArmor() should return string ~"Success"
        /// </summary>
        [Fact]
        public void EquipArmor_CorrectType_Success()
        {
            ArmorType armorType = ArmorType.PLATE;
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Armor testArmor = (Armor)CreateClassesForTests.InitItems(armorType: armorType);
            var actual = warrior.EquipArmor(testArmor);
            // Check if armor got equiped
            Assert.Equal("New armour equipped!", actual);
        }
        #endregion
        #region Calculate character damage
        /// <summary>
        /// Get character damage, Check if damage equal to what we expected.
        /// This test character damage with no items equip.
        /// </summary>
        [Fact]
        public void CalculateDamage_NoItems_Damage()
        {
            // Create default warrior class
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            // Get warrior damage
            var actual = warrior.Damage();
            // What we expect warrior damage to be without any items at level 1
            // Weapon_dmg or 1f *  1 + strenth/100. Strengh is damage attribute for subclass warrior
            float expected = 1f * (1f + (5f / 100f));
            // Check if warrior damage is correct
            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Get character damage with weapon (WeaponType.AXES) equiped, 
        /// Check if damage equal to what we expected.
        /// </summary>
        [Fact]
        public void CalculateDamage_WeaponEquiped_Damage()
        {
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Weapon testAxe = (Weapon)CreateClassesForTests.InitItems(WeaponType.AXES);
            warrior.EquipWeapon(testAxe);
            // Expected damage from warrior at level one with an AXE
            float expected = (7f * 1.1f) * (1f + (5f / 100f));
            var actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }
        /// <summary>
        /// Get character damage with weapon (WeaponType.AXES) and armor (ArmorType.Plate) equiped, 
        /// Check if damage equal to what we expected.
        /// </summary>
        [Fact]
        public void CalculateDamage_WeaponAndArmorEquiped_Damage()
        {
            Character warrior = CreateClassesForTests.InitCharacter(CharacterType.WARRIOR);
            Weapon testAxe = (Weapon)CreateClassesForTests.InitItems(WeaponType.AXES);
            Armor testPlate = (Armor)CreateClassesForTests.InitItems(armorType: ArmorType.PLATE);
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlate);
            // Expected damage from warrior at level 1, with weapon AXES, with armor Plate
            float expected = (7f * 1.1f) * (1f + ((5f + 1f) / 100f));
            var actual = warrior.Damage();

            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
