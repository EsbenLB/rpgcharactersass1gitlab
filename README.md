# RPG Characters
RPG Characters is an application copying class structure from the diablo games  
with c# classes:  
Character, subclass (4 stk),  
Items, armor, weapon  
and tests for those classes. 

### Contributors:
Esben Bjarnason

## Table of Contants
- [Project-status](#project-status)
- [Description](#description)
- [Installation](#installation)
- [Changes](#changes)
- [Diagram](#diagram)
- [Tech-stack](#tech-stack)

### Project-status
...
Done!

### Description:
Project contain of classes that we can run tests on.  
Classes:  
```
1. Character (parent)
  1. Mage (subclass/child)
  2. Warrior
  3. Ranger
  4. Rogue
1. tem (parant)
  1. Weapon (child)
  2. Armor  

Tests:
1. CharacterTests 
1. ItemTests  
```
In project RPGCharacterAss1Tester are tests that we can run on those classes.  
Where we can try to create/levelUp character and equip items.  
Sublcasses can only equip some items and every subclass has unic way of leveling up.  
The tests will check if all methods is implemented the correct way.


### Installation
1. You need Visual studio 2019.
1. Download project.
1. Find .sln file.
1. Dobbel click .Sln file -> open with Visual studio.
1. Right click RPGCharacterAss1Tester and "Run Tests"
1. "Tests"/"Game" is in RPGCharacterAss1Tester Project.
1. F5 to run program file.

### Changes 
1) If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.  
o Use the warrior, and the axe, but set the axes level to 2.
2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.  
o Use the warrior, and the plate body armor, but set the armor’s level to 2.

It throws a InvalidItemException instead  

Why this change?  
We are testing Item attributes instead of Weapon/Armor attributes.

### Tech-stack
1.  Visual studio 2019
1.  C#

### Diagram
1. Open visual studio installer.
1. You need to add individual components to Visual studio called "Class design" to view the diagram.
1. Inside RPGCharacterAss1 folder ClassDiagram1.cd dobbel click on file.
