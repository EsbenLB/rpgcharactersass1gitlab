﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    /// <summary>
    /// CharacterStats contains (strength, dexterity, intelligence).
    /// </summary>
    public class CharacterStats
    {
        public CharacterStats(
            float strength = 0, float dexterity = 0, float intelligence = 0)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }
        /// <summary>
        /// Character attributes
        /// </summary>
        private float strength;
        private float dexterity;
        private float intelligence;
        public float Strength()
        {
            return strength;
        }
        public float Dexterity()
        {
            return dexterity;
        }
        public float Intelligence()
        {
            return intelligence;
        }
        /// <summary>
        /// Pluss operator. Add two classes toghether
        /// </summary>
        /// <param name="stateOne">Itself</param>
        /// <param name="stateTwo">Another class to + with itself</param>
        /// <returns></returns>
        public static CharacterStats operator +(
            CharacterStats stateOne, CharacterStats stateTwo)
        {
            return new CharacterStats
            {
                strength = stateOne.strength + stateTwo.strength,
                dexterity = stateOne.dexterity + stateTwo.dexterity,
                intelligence = stateOne.intelligence + stateTwo.intelligence,
            };
        }
    }
}