﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    /// <summary>
    /// See Mage class for comments
    /// </summary>
    public class Ranger : Character
    {
        public Ranger(string name = "DefaultName") :
            // Character starting stats
            base(name, CharacterType.WARRIOR, new CharacterStats(1, 7, 1), 
            // Can equip item list
            new WeaponType[] { WeaponType.BOWS, },
            new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL }
        )
        {  }
        public override CharacterStats GetLevelUpStatsForSubclass()
        {
            return new CharacterStats(1, 5, 1);
        }
        public override float GetSubclassDamageStat()
        {
            return this.GetTotalStats().Dexterity();
        }
    }
}