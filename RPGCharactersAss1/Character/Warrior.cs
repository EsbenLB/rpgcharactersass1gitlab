﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    /// <summary>
    /// See Mage class for comments
    /// </summary>
    public class Warrior : Character
    {
        public Warrior(string name = "DefaultName") :
            // Character starting stats
            base(name, CharacterType.WARRIOR, new CharacterStats(5, 2, 1), 
            // Can equip item list
            new WeaponType[] { WeaponType.AXES,  WeaponType.SWORDS, WeaponType.SWORDS },
            new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE }
        )
        {  }
        public override CharacterStats GetLevelUpStatsForSubclass()
        {
            return new CharacterStats(3, 2, 1);
        }
        public override float GetSubclassDamageStat()
        {
            return this.GetTotalStats().Strength();
        }
    }
}