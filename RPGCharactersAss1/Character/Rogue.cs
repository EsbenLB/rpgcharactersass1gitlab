﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    /// <summary>
    /// See Mage class for comments
    /// </summary>
    public class Rogue : Character
    {
        public Rogue(string name = "DefaultName") :
            // Character starting stats
            base(name, CharacterType.WARRIOR, new CharacterStats(2, 6, 1), 
            // Can equip item list
            new WeaponType[] { WeaponType.DAGGERS,  WeaponType.SWORDS },
            new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL }
        )
        {  }
        public override CharacterStats GetLevelUpStatsForSubclass()
        {
            return new CharacterStats(1, 4, 1);
        }
        public override float GetSubclassDamageStat()
        {
            return this.GetTotalStats().Dexterity();
        }
    }
}