using System;
using RPGCharactersAss1;

namespace RPGCharactersAss1Tester
{
    /// <summary>
    /// Create default character and weapons.
    /// Makes it easier to create character and weapons for tests.
    /// </summary>
    public class CreateClassesForTests
    {
        /// <summary>
        /// Send enum, receive character.subclass, e.g. = warrior
        /// </summary>
        /// <param name="characterType">Specify subclass to get</param>
        /// <returns></returns>
        public static Character InitCharacter(CharacterType characterType)
        {
            Character character = characterType switch
            {
                // No subclass of type NONE
                CharacterType.NONE => throw new InvalidOperationException("Can't create this subclass" + characterType.ToString()),
                // Create subclass
                CharacterType.MAGE => new Mage(),
                CharacterType.WARRIOR => new Warrior(),
                CharacterType.RANGER => new Ranger(),
                CharacterType.ROGUE => new Rogue(),
                // Default
                _ => throw new InvalidOperationException("Can't create this subclass" + characterType.ToString()),
            };
            // return a subclass
            return character;
        }
        /// <summary>
        /// Send WeaponType or ArmorType, return default weapon or armor  
        /// </summary>
        /// <param name="weaponType">Type of weapon you want</param>
        /// <param name="armorType">Type of armopr you want</param>
        /// <returns>Weapon or Armor</returns>
        public static Item InitItems(WeaponType weaponType = WeaponType.NONE, ArmorType armorType = ArmorType.NONE)
        {
            // If WeaponType/ArmorType != null, Create that type of item
            Item item = new Weapon(EquipmentType.NONE, WeaponType.NONE);
            if(weaponType != WeaponType.NONE)
            {
                item = weaponType switch
                {
                    // Default weapons
                    WeaponType.NONE => throw new InvalidOperationException("Can't create this item" + weaponType.ToString()),
                    WeaponType.AXES => new Weapon(EquipmentType.WEAPON, WeaponType.AXES, "Common axe", 1, 7, 1.1f),
                    WeaponType.BOWS => new Weapon(EquipmentType.WEAPON, WeaponType.BOWS, "Common bow", 1, 12, 0.7f),
                    _ => throw new InvalidOperationException("Can't create this item" + weaponType.ToString()),
                };
            }
            else if (armorType != ArmorType.NONE)
            {
                item = armorType switch
                {
                    // Default armors
                    ArmorType.NONE => throw new InvalidOperationException("Can't create this item" + armorType.ToString()),
                    ArmorType.PLATE => new Armor(EquipmentType.BODY, ArmorType.PLATE, "Common plate body armor", 1, new(strength: 1)),
                    ArmorType.CLOTH => new Armor(EquipmentType.HEAD, ArmorType.CLOTH, "Common cloth head armor", 1, new(intelligence: 1)),
                    _ => throw new InvalidOperationException("Can't create this item" + armorType.ToString()),
                };
            }
            // return weapon or armor
            return item;
        }

    }
}
