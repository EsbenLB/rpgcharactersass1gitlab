﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    public enum CharacterType
    {
        NONE,
        MAGE,
        RANGER,
        ROGUE,
        WARRIOR
    }
    public abstract class Character
    {
        // Character name
        private readonly string name;
        // start at level 1
        private int level = 1;
        // Character Subclass e.g. Mage, Warrior
        private readonly CharacterType curCharacterType;
        // Base attributes
        private CharacterStats stats;
        // Character equiped items
        public Dictionary<EquipmentType, Item> equipment =
            new();

        // Can equip for this subclass. List of weapon and armor
        private readonly WeaponType[] canEquipWeapon = { };
        private readonly ArmorType[] canEquipArmor = { };

        #region Get Variables
        public string Name()
        {
            return name;
        }
        public int Level()
        {
            return level;
        }
        public CharacterType CurCharacterType()   // property
        {
            return curCharacterType;
        }
        public CharacterStats Stats()
        {
            return stats;
        }
        // Total attributes
        public CharacterStats GetTotalStats()
        {
            CharacterStats itemTotalStats = stats;
            foreach (var item in equipment)
            {
                if(item.Value.GetType() == typeof(Armor))
                {
                    itemTotalStats += (item.Value as Armor).getArmorStates();
                }
            }
            return itemTotalStats;
        }
        #endregion
        /// <summary>
        /// Character construtor get used in subclass construtor.
        /// </summary>
        /// <param name="name">character name</param>
        /// <param name="curCharacterType">character subclass</param>
        /// <param name="stats">character subclass</param>
        /// <param name="canEquipWeapon">what weapon subclass can equip</param>
        /// <param name="canEquipArmor">what armor subclass can equip</param>
        protected Character(
            string name, CharacterType curCharacterType, CharacterStats stats
            , WeaponType[] canEquipWeapon
            , ArmorType[] canEquipArmor
            )
        {
            this.name = name;
            this.curCharacterType = curCharacterType;
            this.stats = stats;
            this.canEquipWeapon = canEquipWeapon;
            this.canEquipArmor = canEquipArmor;
        }
        #region abstract methods: LevelUp & Damage
        /// <summary>
        /// Get Subclass unic characterStats when leveling up.
        /// </summary>
        /// <returns>Subclass Stats for leveling up</returns>
        public abstract CharacterStats GetLevelUpStatsForSubclass();
        /// <summary>
        /// Level up character, level++, CharacterStats += LevelUpSubclassStats
        /// </summary>
        public void LevelUp()
        {
            level++;
            this.stats += GetLevelUpStatsForSubclass();

        }
        /// <summary>
        /// Get subclass attack stat, either strength or dex... or Intell...
        /// subclass attack stat = e.g. strength from totalAttributes.
        /// </summary>
        /// <returns></returns>
        public abstract float GetSubclassDamageStat();
        /// <summary>
        /// Calc damage. Adds attack damage from Weapon if equiped, else 1.
        /// Weapon DPS * (1 + TotalPrimaryAttribute/100)
        /// </summary>
        /// <returns>Character damage</returns>
        public float Damage()
        {
            float itemDmg = 0;
            // If no weapon. Attack = 1
            if(equipment.ContainsKey(EquipmentType.WEAPON) == false) { itemDmg = 1; } 
            else
            {
                // Check/Support multiple weapons instead of just one
                foreach (var item in equipment)
                {
                    // If item is weapon type
                    if (item.Value.GetType() == typeof(Weapon))
                    {
                        itemDmg += (item.Value as Weapon).Damage();
                    }
                }
            }
            return itemDmg * (1 + (GetSubclassDamageStat() / 100));
        }
        #endregion
        #region Equip
        /// <summary>
        /// Check if item level same/below character level. Then equip item.
        /// else throw Exception
        /// <exception cref="InvalidItemException">Thrown when Weapon level to high for character.</exception>
        /// </summary>
        /// <param name="item"></param>
        private void EquipItem(Item item)
        {
            if (this.level >= item.GetLevel())
            {
                if (equipment.ContainsKey(item.GetEquipmentType()))
                {
                    equipment[item.GetEquipmentType()] = item;
                }
                else
                {
                    equipment.Add(item.GetEquipmentType(), item);   
                }
            }
            else
            {
                throw new InvalidItemException("Weapon level too high. Can't Equip");
            }

        }
        /// <summary>
        /// Check if character can equip weapon type. Then try to equip weapon.
        /// Else throw Exception.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="InvalidArmorException">Thrown when character can't equipt weapon bechoose of weaponType</exception>
        /// <returns>~"Success"</returns>
        public string EquipWeapon(Weapon item)
        {
            if (this.canEquipWeapon.Contains(item.GetWeaponType())){
                EquipItem(item);
                return "New weapon equipped!";
            }
            else
            {
                throw new InvalidWeaponException(item.GetWeaponType().ToString() + " Can't equipt this type of weapon");
            }
        }
        /// <summary>
        /// Check if character can equip armor type. Then try to equip armor.
        /// Else throw Exception.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="InvalidArmorException">Thrown when character can't equipt armor bechoose of weaponType</exception>
        /// <returns>~"Success"</returns>
        public string EquipArmor(Armor item)
        {
            if (this.canEquipArmor.Contains(item.getArmorType()))
            {
                EquipItem(item);
                return "New armour equipped!";
            }
            else
            {
                throw new InvalidArmorException(item.getArmorType().ToString() + " Can't equipt this type of Armor");
            }
        }
        /// <summary>
        /// Add character stats to a stringbuilder, Then return stringvuilder 
        /// </summary>
        /// <returns>Stringbuilder with character stats</returns>
        #endregion
        public StringBuilder DisplayCharacter()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine("Name: " + this.name);
            sb.AppendLine("Level: " + this.level);

            sb.AppendLine("");
            sb.AppendLine("Total Stats: ");
            sb.AppendLine("\tStrength: " + this.GetTotalStats().Strength());
            sb.AppendLine("\tDexterity: " + this.GetTotalStats().Dexterity());
            sb.AppendLine("\tIntelligence: " + this.GetTotalStats().Intelligence());
            sb.AppendLine("Damage: " + this.Damage());

            return sb;
        }
    }
}