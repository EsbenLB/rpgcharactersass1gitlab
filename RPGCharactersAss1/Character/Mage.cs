﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    /// <summary>
    /// Character subclass
    /// </summary>
    public class Mage : Character
    {
        public Mage(string name = "DefaultName") :
            // Character starting stats
            base(name,  CharacterType.MAGE, new CharacterStats(1, 1, 8), 
            // Can equip item list
            new WeaponType[] { WeaponType.STAFFS,  WeaponType.WANDS, },
            new ArmorType[] { ArmorType.CLOTH }
        )
        {  }
        /// <summary>
        /// Get CharacterStats that character gets when leveling up.
        /// </summary>
        /// <returns>CharacterStats for leveling up</returns>
        public override CharacterStats GetLevelUpStatsForSubclass()
        {
            return new CharacterStats(1, 1, 5);
        }
        /// <summary>
        /// Get character attack stats. Either streng or dex... or intell...
        /// </summary>
        /// <returns></returns>
        public override float GetSubclassDamageStat()
        {
            return this.GetTotalStats().Intelligence();
        }
    }
}