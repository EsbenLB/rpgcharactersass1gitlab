﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersAss1
{
    /// <summary>
    /// Type of Armor or Weapon
    /// </summary>
    /// Serval classes uses EquipmentType
    public enum EquipmentType
    {
        NONE,
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

}
