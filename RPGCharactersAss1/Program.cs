﻿using RPGCharactersAss1Tester;
using System;

namespace RPGCharactersAss1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Right click RPGCharacterAss1Tester and "Tun Tests" 
            // "Code"/"Game" is in RPGCharacterAss1Tester Project
            Warrior mageBob = new("bob");
            mageBob.LevelUp();
            mageBob.EquipWeapon((Weapon)CreateClassesForTests.InitItems(WeaponType.AXES));

            Console.WriteLine(mageBob.DisplayCharacter().ToString());
            Console.WriteLine("End");
        }
    }
}
