﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersAss1
{
    /// <summary>
    /// Exception for failing to equip Item on character.
    /// </summary>
    public class InvalidItemException : Exception
    {
        public InvalidItemException() 
        {
        }
        public InvalidItemException(string message): base(message)
        {
        }
        public override string Message => "Custom error";
    }
}
