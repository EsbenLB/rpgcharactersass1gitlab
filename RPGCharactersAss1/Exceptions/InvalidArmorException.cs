﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersAss1
{
    /// <summary>
    /// Exception for failing to equip Armor on character.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() 
        {
        }
        public InvalidArmorException(string message): base(message)
        {
        }
        public override string Message => "Custom error";
    }
}
