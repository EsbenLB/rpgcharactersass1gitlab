﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersAss1
{
    /// <summary>
    /// Exception for failing to equip Weapon on character.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() 
        {
        }
        public InvalidWeaponException(string message): base(message)
        {
        }
        public override string Message => "Custom error";
    }
}
