﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    // Type of armor.
    public enum ArmorType
    {
        NONE,
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
    public class Armor : Item
    {
        private readonly ArmorType curArmoType;
        // Armor stats 
        private readonly CharacterStats itemStat = new();

        public Armor(
            EquipmentType curEquipmentType, ArmorType curArmoType,  string itemName = "defaultName",
            int itemLevel = 1,  CharacterStats itemStat = null
            ) 
            : base(itemName, itemLevel, curEquipmentType)
        {
            // If itemStat = null, Don't set this.itemStat
            this.curArmoType = curArmoType;
            if(itemStat != null) 
                { this.itemStat = itemStat; }
            
        }
        /// <summary>
        /// Get CharacterStats on Armor.
        /// </summary>
        /// <returns>CharacterStats from armor</returns>
        public CharacterStats getArmorStates()
        {
            return this.itemStat;
        }
        /// <summary>
        /// Get armor type.
        /// </summary>
        /// <returns>armor type</returns>
        public ArmorType getArmorType()
        {
            return curArmoType;
        }
        /// <summary>
        /// Display armor stats.
        /// </summary>
        /// <returns>String with armor stats</returns>
        public override string GetDisplayItemSubclass()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine("\tArmorType: " + curArmoType);
            sb.AppendLine("\tStrength: " + itemStat.Strength());
            sb.AppendLine("\tDexterity: " + itemStat.Dexterity());
            sb.AppendLine("\tIntelligence: " + itemStat.Intelligence());
            return sb.ToString();
        }
    }
    
}