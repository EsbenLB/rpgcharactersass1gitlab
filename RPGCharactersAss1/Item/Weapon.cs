﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;




namespace RPGCharactersAss1
{
    // WeaponType
    public enum WeaponType
    {
        NONE,
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS,
    }   
    public class Weapon : Item
    {
        private readonly WeaponType curWeaponType;
        // Weapon damage
        private readonly float baseDmg = 0;
        private readonly float atkSpeed = 0;


        public Weapon(
            EquipmentType curEquipmentType,
            WeaponType curWeaponType, string itemName = "DefaultName", int itemLevel = 1,  float baseDmg = 1, float atkSpeed = 1.1f
            ) : base(itemName, itemLevel, curEquipmentType)
        {
            this.curWeaponType = curWeaponType;
            this.baseDmg = baseDmg;
            this.atkSpeed = atkSpeed;
        }
        public float BaseDmg()
        {
            return baseDmg;
        }
        public float AtkSpeed()
        {
            return atkSpeed;
        }
        public float Damage()
        {
            return baseDmg * atkSpeed;
        }
        public WeaponType GetWeaponType()
        {
            return curWeaponType;
        }
        /// <summary>
        /// Display weapon stats.
        /// </summary>
        /// <returns>String with weapon stats</returns>
        public override string GetDisplayItemSubclass()
        {
            StringBuilder sb = new(50);
            sb.AppendLine("\tWeaponType: " + curWeaponType);
            sb.AppendLine("\tbaseDmg: " + baseDmg);
            sb.AppendLine("\tatkSpeed: " + atkSpeed);
            return sb.ToString();
        }

    }
}