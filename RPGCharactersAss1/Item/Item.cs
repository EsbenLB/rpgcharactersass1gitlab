﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharactersAss1
{
    public abstract class Item
    {
        
        // readonly: set value: declaration or in a constructor in the same class.
        private readonly string itemName; 
        private readonly int itemLevel;
        // Where to equip item e.g. Head, Body...
        private readonly EquipmentType curEquipmentType;

        protected Item(string itemName, int itemLevel, EquipmentType curEquipmentType)
        {
            this.itemName = itemName;
            this.itemLevel = itemLevel;
            this.curEquipmentType = curEquipmentType;
        }


        public string GetName()
        {
            return itemName;
        }
        public int GetLevel()
        {
            return itemLevel;
        }
        public EquipmentType GetEquipmentType()
        {
            return curEquipmentType;
        }
        /// <summary>
        /// Display item stats.
        /// </summary>
        /// <returns>String with item stats</returns>
        public abstract string GetDisplayItemSubclass();
        public string DisplayItem()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine("   Name: " + this.itemName);
            sb.AppendLine("\tLevel: " + this.itemLevel);
            sb.AppendLine("\tEquipmentType: " + this.curEquipmentType);
            sb.AppendLine(GetDisplayItemSubclass());
            return sb.ToString();
        }
    }
}